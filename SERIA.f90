  PROGRAM Atrapandora
  implicit real(a-h,o-z)
  external ran2 
  character*6 QNT,Mode
  integer Tree(4), TRK, rec
  real   pTree(4),CMX(2),Stot(4)
  real*8 ran2
  integer ADIC(0:105,0:105,4)
  integer, allocatable, dimension(:) ::   STT,SAI,SAT,RST,ICU,MOV,ISO,FMLk
  integer, allocatable, dimension(:) ::   BT,ST0,STD,SPS,RGS,FMI,FMLm,DWL
  integer, allocatable, dimension(:,:) :: R0,CTT,FMLi,CNT
  real, allocatable, dimension(:) ::   DCD,DCDp,C1W,C2W
  real, allocatable, dimension(:,:) :: ATR,CNT2
  integer COL(10000)
  common/Probs/PSP,PCA,SCF,PRM,PCT,PAE,PTD,MIC,nPD,DLY
  common/CPDp/aa,bb
  common/VL/Pa1, Pa2, Pa3, xnPD
  common/CB/B0a0,B0a1,B1a0,B1a1  

  
  open(1,file='Tar')
  open(10,file='Pop-States.dat')
!   open(17,file='Capac-Hosp.dat')
!   open(30,file='Hist-Cntg-I-1.dat')
!   open(31,file='Hist-Cntg-A-1.dat')
  open(50,file='Hist-Cntg-I.dat')
  open(51,file='Hist-Cntg-A.dat')
  open(11,file='Contacts.dat')
  open(12,file='Inf-Age.dat')
  open(13,file='Inf-Age2.dat')
  open(70,file='ADIC-soc-repo.dat')
  open(71,file='ADIC-soc-real.dat')
  open(72,file='ADIC-fml-repo.dat')
  open(73,file='ADIC-fml-real.dat')
  open(74,file='ADIC-all-repo.dat')
  open(75,file='ADIC-all-real.dat')
  open(94,file="STT-ATR.dat")
  open(95,file="FML.dat")

  pi=acos(-1.d0)
  Per=2.*pi
  B0a0 = 0.492438; B0a1 = 0.000602776
!   B1a0 = 0.0688638; B1a1 = 0.00384014   ! School Open
  B1a0 = 0.0088638; B1a1 = 0.00216654     ! School Close  
  
! ------------------------------------------------- Variables
! All Prob. & Percent. are in the scale [0-1] 
  read(1,*); read(1,*)
  read(1,*)nPD
  read(1,*)mpf
  read(1,*)IDUM
  read(1,*); read(1,*)
  read(1,*)CPD
  read(1,*)DOI
  read(1,*)DOS
  read(1,*)DA1
  read(1,*)DFR
  read(1,*); read(1,*)
  read(1,*)Nstep
  read(1,*)N0
  read(1,*)NE0
  read(1,*)NS0
  read(1,*)SCF      ! Super-Contagion Factor. Not used in this version
  read(1,*)QNT
  read(1,*)PQN
  read(1,*)PAE
  read(1,*)NTT
  read(1,*)TRK
  read(1,*)rec
  read(1,*)PCT
  read(1,*)PTD
  read(1,*)DLY
  read(1,*)RMax
  read(1,*)
  read(1,*)PSP
  read(1,*)PCA
  read(1,*)PRM 
  read(1,*)CHC
  read(1,*)aa
  read(1,*)bb
  read(1,*)b3
  read(1,*)RSC        ! Super-contag. Not used in this version
  read(1,*)CMX(1)
  read(1,*)DWLp
  read(1,*)RPI
  read(1,*)RPA
  read(1,*)Mode
  
    Pa1=0.2*sqrt(2.*pi) ! Infectiousness-time parameters
    Pa2=0.97
    Pa3=2.*0.75**2
    xnPD=float(nPD)
  nalm=nint(0.9*float(Nstep))
  
 ! -------------------------------------------------
  MIC=nint(CHC*float(N0))

  allocate(ATR(N0,9))
  allocate(STT(N0))
  allocate(CNT(N0,2))       ! (i,1) Number of direct contacts   | (i,2) Idem registered by app
  allocate(CNT2(N0,2))      ! (i,1) Number of indirect contacts | (i,2) Idem registered by app
  allocate(C1W(N0))         ! Average direct contacts per day
  allocate(C2W(N0))
  allocate(SAI(N0))         ! Counts steps after infection
  allocate(SAT(N0))
  allocate(RST(N0))
  allocate(ICU(N0))
  allocate(MOV(N0))
  allocate(ISO(N0))         ! ISO(i)=0/1  has/hasn's been tracked & isolated
  allocate(R0(N0,2))        ! R0(:,1) for I | R0(:,2) for A
  allocate(BT(N0))          ! BT(i)    Number of agents infected by "i" (no diff. I & A)  
  allocate(CTT(N0,299))     ! CTT(i,:) ID of the agents infected by "i"
  allocate(ST0(N0))         ! =0 (R or M) that was I     =1 (R or M) that was A
  allocate(STD(N0))         ! Determines if i will be I [STD(i)=0] or A [STD(i)=1]
  allocate(SPS(N0))         !=0 Normal | =1 Superspreader
  allocate(RGS(N0))         ! Registro de I tot. + A rastreados por CT
  allocate(FMI(N0))         ! FMI(i) Number of family members of agent "i"
  allocate(FMLm(N0))        ! FMLm(k) Number of family members of family k
  allocate(FMLi(N0,9))      ! FMLi(k,j)=i ID of each family member
  allocate(FMLk(N0))        ! Which familiy does i belong to
  allocate(DCD(N0))         ! Decrease in daily contacts
  allocate(DCDp(N0))
  allocate(DWL(N0))         ! 1:Downloaded the App  0:Didn't
  
!   write(17,*)0., CHC*100.
!   write(17,*)float(Nstep)/float(nPD), CHC*100.
    
  if(Mode.eq."calc".or.Mode.eq."write")then
  
        call Stats(IDUM,N0,nPD,DOI,PCA,  STD,ATR)
        
        call mkFML(N0,IDUM,Nfm,FMLm,FMLi,FMLk,FMI,ATR)
!         stop  ! WARNING
        
        call InitState(QNT,IDUM,Nq,N0,NE0,NS0,PQN,RMax,b3,   &
                        DWLp,DWL,DCDp,BT,R0,ATR,MOV,STT,ST0,SPS,SAI,CTT,Tree)
                        
        if(Mode.eq."write")call WriteAll(N0,Nfm,FMLm,FMLi,FMLk,FMI,STT,ST0,STD,SAI,SPS,MOV,ATR)
        
  elseif(Mode.eq."read")then
        print*,"Reading Library"
        call ReadAll(IDUM,N0,Nfm,FMLm,FMLi,FMLk,FMI,b3,STT,ST0,STD,SAI,SPS,MOV,   &
                     DWLp,DWL,DCDp,BT,R0,CTT,ATR)
  else
        print*,"Mode must be 'calc', 'read', or 'write'. Stopping..."
  endif
  
                 
  print*, "Initial state done!"
  
!   print*; print*,"-------   N0      NE0      ICU      %QNT    "
!   write(*,'(8x,i6,4X,i6,4X,i4,7X,F5.2)')N0, NE0, MIC, PQN*100.

  print*; print*,"-------   N0         NE0    Nfm%"
  write(*,'(8x,i6,4X,i6,4X,F5.2)')N0, NE0, 100.*float(Nfm)/float(N0)
!   stop
!                                 print*,"Exposed:"
!                                 ic=0
!                                 do i=1,N0
!                                     if(STT(i).eq.2)then
!                                     ic=ic+1
!                                     write(86,*)ic,i
!                                     endif
!                                 enddo
!                                 stop
  
                 
  call PrintStats(pi,PSP)
  stop
    
  CNT=0               ! Number direct contacts  | real & in the App
  CNT2=0.             ! Number indirect contacts| real & in the App
  C1W=0.
  DCD=1.
  SAT=-1              ! Steps after Testing
  ICU=0
  NIS=0               ! extra ICUs needed (above Hospital Capacity)
  ISO=0               ! No one has been tracked & isolated
!   NMT=nint(float(N0)*PTD) !Pop. tested per day
  NFL=0               ! switch ON OFF once
  ADIC=0              ! ADIC(x,y,k) Age distrib. contagion x=contact y=infected 
                      !k: 1:Social-real  2:Social-reported  3:FML-real   4:FML-reported
  NctgF=0             ! Total Family (in-home) contagion
  MRT=0
  CPH=CPD/float(nPD)  ! Contacts per hour
  NCmin=0.5*float(N0)*CPH
  print*,"NCmin",NCmin
    
    
! ------------------------------------------------- Start Simulation  
  DO ist=1,Nstep    ! Each step = one hunger game at the arena
        
        call MOVE(ist,IDUM,N0,MRT,PRM,CPH,NCmin,ATR,R0,BT,SAI,DCD,CTT,  &
                  FMLk,FMI,RPI,RPA,DWL,CNT,CNT2,C1W,STT,SPS,ADIC)
        
        call Evol(IDUM,ist,N0,NIS,STT,ST0,STD,SPS,RST,MOV,ATR,QNT,  &
                  R0,SAT,SAI,ICU)
                  
        ma=mod(ist,nPD)
        if(ma.eq.0) call FMLctg(IDUM,N0,Nfm,NctgF,STT,PRM,FMLm,FMLi,ATR,  &
                                R0,BT,SAI,CTT,RPI,RPA,ADIC)
        
        !------------------------  All scenarios perform mCT (except non!=0)
        if(NTT.gt.0.and.RPI.gt.1.E-2)then
            do i=1,N0
                if(ST0(i).eq.0)then ! it is or it was I
                    na=nint(ATR(i,4)+(PSP+DLY)*nPD)
                    if(SAI(i).eq.na)then ! DLY from sympt has passed
                        r=ran2(IDUM)
                        if(r.le.RPI)then
                            call mkCTm(i,N0,IDUM,rec,CTT,STT,DWL,MOV,BT,Tree, &
                                       QNT,FMLk,  ADIC,RGS,ATR)
                        endif
                    endif
                elseif(RPA.gt.1.E-2.and.ST0(i).eq.1)then                 ! it is or it was A
                    na=nint(ATR(i,4)+(ATR(i,9)+DLY)*nPD)
                    if(SAI(i).eq.na)then ! DLY from sympt has passed
                        r=ran2(IDUM)
                        if(r.le.RPA)then
                            call mkCTm(i,N0,IDUM,rec,CTT,STT,DWL,MOV,BT,Tree, &
                                       QNT,FMLk,  ADIC,RGS,ATR)
                        endif
                    endif
                endif
            enddo
        else
            if(RPA.gt.1.E-2)then                ! No mCT, vCT or CP applied        
            do i=1,N0
                    if(ST0(i).eq.1)then ! it is or it was A
                        na=nint(ATR(i,4)+(ATR(i,9)+DLY)*nPD)
                        if(SAI(i).eq.na)then
                            r=ran2(IDUM)
                            if(r.le.RPA.and.STT(i).le.2)ATR(i,1)=0.
                        endif
                    endif
            enddo
            endif
        endif
        
        !------------------------ CT for IDR1,2,3
        if(NTT.eq.2)then
        if(TRK.eq.1)then  
            do i=1,N0
                if(ST0(i).eq.0)then ! it is or it was I
                    na=nint(ATR(i,4)+(PSP+DLY)*nPD)
                    if(SAI(i).eq.na)then ! DLY from sympt has passed
                        r=ran2(IDUM)
                        if(r.le.RPI)then
                            call mkCT(i,N0,IDUM,rec,CTT,STT,DWL,MOV,BT,Tree, &
                                      QNT,FMLk,  ADIC,RGS,ATR)
                        endif
                    endif
                elseif(RPA.gt.1.E-2.and.ST0(i).eq.1)then                 ! it is or it was A
                    na=nint(ATR(i,4)+(ATR(i,9)+DLY)*nPD)
                    if(SAI(i).eq.na)then ! DLY from sympt has passed
                        r=ran2(IDUM)
                        if(r.le.RPA)then
                            call mkCT(i,N0,IDUM,rec,CTT,STT,DWL,MOV,BT,Tree,QNT,FMLk,  ADIC,RGS,ATR)
                        endif
                    endif
                endif
            enddo
        
        !------------------------ CP for IDR1,2
        elseif(TRK.eq.2)then
        
            if(ma.eq.0) &
            call SMS(IDUM,N0,nPD,ist,rec,CNT,CNT2,C1W,C2W,FMI,FMLk,FMLi,CMX,DWL,DWLp,DCDp,  DCD)
                        
        !------------------------ MIX ofr IDR1,2,3
        elseif(TRK.eq.3)then
        
            do i=1,N0
                if(ST0(i).eq.0)then ! it is or it was I
                    na=nint(ATR(i,4)+(PSP+DLY)*nPD)
                    if(SAI(i).eq.na)then ! DLY from sympt has passed
                        r=ran2(IDUM)
                        if(r.le.RPI)then
                            call mkCT(i,N0,IDUM,rec,CTT,STT,DWL,MOV,BT,Tree, &
                                      QNT,FMLk,  ADIC,RGS,ATR)
                        endif
                    endif
                elseif(RPA.gt.1.E-2.and.ST0(i).eq.1)then                 ! it is or it was A
                    na=nint(ATR(i,4)+(ATR(i,9)+DLY)*nPD)
                    if(SAI(i).eq.na)then ! DLY from sympt has passed
                        r=ran2(IDUM)
                        if(r.le.RPA)then
                            call mkCT(i,N0,IDUM,rec,CTT,STT,DWL,MOV,BT,Tree, &
                                      QNT,FMLk,  ADIC,RGS,ATR)
                        endif
                    endif
                endif
            enddo
            
            if(ma.eq.0) &
            call SMS(IDUM,N0,nPD,ist,rec,CNT,CNT2,C1W,C2W,FMI,FMLk,FMLi,CMX,DWL,DWLp,DCDp,  DCD) 
            
        endif
        endif
        
        
        n=mod(ist,mpf)     ! Show every mpf steps
        if(n.eq.0)call DATthis(ist,mpf,N0,MRT,MIC,NIS,Tree,ICU,nPD,STT,  &
                               SPS,SAI,ATR,RGS,R0,CTA)
                               
        if(ist.eq.nalm)then
!         if(CTA.gt.16..and.NFL.eq.0)then
!             NFL=1
            call DATthis2(N0,ist,nPD,ST0,FMI,FMLk,FMLi,CMX,CNT,CNT2,C1W,C2W,ATR)  
        endif
!         
        AVG_ct=sum(CNT(:,1))/float(N0*ist)    ! contacts per step per agent avg
!         write(11,*)ist, AVG_ct*float(nPD)   ! contacts per day  per agent avg
  ENDDO
  
    print*; print*, "Simulation done"; print*
    
    m=0; mn=0; n=0
    do i=1,N0
        if(STT(i).eq.4)then
            m=m+1
            if(ATR(i,1).gt.1.E-6)mn=mn+1
        endif
        if(ATR(i,1).gt.1.E-6)n=n+1
!         if(MOV(i).gt.1)n=n+1      ! CHECK
    enddo
    
    ! ----------------------- Contagion per agent Histogram - final
    ncta=N0   ! FES: (N0-S)*100./N0
    do i=1,N0
    
        if(ST0(i).eq.0)then     ! i is or was I
            if(STT(i).eq.4.or.STT(i).eq.-1)write(50,*)BT(i)
        elseif(ST0(i).eq.1)then ! i is or was A
            if(STT(i).eq.4.or.STT(i).eq.-1)write(51,*)BT(i)
        endif
        
        if(STT(i).eq.3)ncta=ncta-1
        
    enddo 
    
    ! ----------------------- Age distribution of index cases
    ! 1:Social-real  2:Social-reported  3:FML-real   4:FML-reported
    nw=69
    do k=1,4
        nw=nw+1
        Stot(k)=sum(ADIC(:,:,k))
        do j=0,80
            do i=0,80
                rel=100.*ADIC(i,j,k)/Stot(k)
                write(nw,*)i, j, rel
            enddo
            write(nw,*)
        enddo
    enddo
    print*,"soc-CT  ",nint(Stot(1))
    print*,"soc-real",nint(Stot(2))
    print*,"fml-CT  ",nint(Stot(3))
    print*,"fml-real",nint(Stot(4))
    print*,"                          soc: CT/real",real(Stot(1)/Stot(2))
    print*,"                          fml: CT/real",real(Stot(3)/Stot(4)); print*
    
    ! ----------------------- ADIC total reported
    nw=nw+1
    Stot(1)=sum(ADIC(:,:,1))+sum(ADIC(:,:,3))
    do j=0,80
        do i=0,80
            rel=100.*(ADIC(i,j,1)+ADIC(i,j,3))/Stot(1)
            write(nw,*)i, j, rel
        enddo
        write(nw,*)
    enddo  
    
    ! ----------------------- ADIC total real
    nw=nw+1
    Stot(1)=sum(ADIC(:,:,2))+sum(ADIC(:,:,4))
    do j=0,80
        do i=0,80
            rel=100.*(ADIC(i,j,2)+ADIC(i,j,4))/Stot(1)
            write(nw,*)i, j, rel
        enddo
        write(nw,*)
    enddo 
        
    
! !     ! ----------------------- 
!     pTree(:)=100.*float(Tree(:))/float(Tree(1))
! !     write(20,*)pTree(:)
    
    
    
!     print*, "Recovered Population  ", real(float(m)/float(N0)*100.)
!     print*, "Rec. Mov. Population  ", real(float(mn)/float(N0)*100.)
    print*, "Contagiados Acumulados", real(float(ncta)/float(N0)*100.)
    print*, "Family Contagion      ", real(float(NctgF)/float(ncta)*100.)
    
!     print*, "Movile Population     ", real(float(n)/float(N0)*100.)
!     print*, "        Tree          ", real(pTree(:)) ;print*
    write(1,*)IDUM

  END

    
  

  
  
  
! XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
! XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
! XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
  
!   STT(n)=4   Cured
!   STT(n)=3   Susceptible
!   STT(n)=2   Exposed  :   No contagia | No tiene síntomas
!   STT(n)=1   Asymptom :      Contagia | No tiene síntomas (Portador Asintomático)
!   STT(n)=0   Infected :      Contagia | Tiene síntomas    (Portador Sintomático)
!   STT(n)=-1  Dead

  SUBROUTINE MOVE(ist,IDUM,N0,MRT,PRM,CPH,NCmin,ATR,R0,BT,SAI,DCD,CTT,  &
                  FMLk,FMI,RPI,RPA,DWL,CNT,CNT2,C1W,STT,SPS,ADIC)
  implicit real(a-h, o-z)
  external ran2
  real*8 ran2
  integer STT(N0), CNT(N0,2), R0(N0,2),BT(N0),DWL(N0),FMI(N0),FMLk(N0)
  integer CTT(N0,299),SPS(N0),SAI(N0),ADIC(0:105,0:105,4)
  real ATR(N0,9), DCD(N0), CNT2(N0,2), C1W(N0)
  common/CB/B0a0,B0a1,B1a0,B1a1
    
! --------------------------- Draw contacts    
    nc=0
    do while (nc.lt.NCmin)
    800 i=int(ran2(IDUM)*float(N0))+1; if(STT(i).eq.-1)goto 800
    801 j=int(ran2(IDUM)*float(N0))+1; if(STT(j).eq.-1)goto 801
    
    ! if i-j are cohabitants, their contact is not considered (FML contact)
    k1=FMLk(i); k2=FMLk(j)
    if(i.ne.j.and.k1.ne.k2)then
!--------------------------------------- Contact Probability
        !Prob=1.0    !MEGAWARNING 0.8
        
        dA=abs(ATR(j,8)-ATR(i,8))
!         PP=ATR(i,7)*ATR(j,7)   *(0.75*exp(-0.1*dA**2)+0.25)                ! Version0 (Overleaf)
!         PP=ATR(i,7)*ATR(j,7)   *(0.692441*exp(-0.03792*dA**2)+1.-0.692441) ! Version1 (single f2)

        x=0.5*(ATR(j,8)+ATR(i,8))                                ! Version2 (f2(age))  
        B0 = B0a0*exp(-B0a1*x**2)+0.46
        B1 = B1a0*exp(-B1a1*x**2)+0.014
        
        PP=ATR(i,7)*ATR(j,7)   *(B0*exp(-B1*dA**2)+1.-B0)

!             ! CNT(i,1):  1w contacts
!             ! CNT(i,2):  1w contacts detected by the App
!             ! CNT2(i,1): 2w contacts 
!             ! CNT2(i,2): 2w contacts detected by the App
        r1=ran2(IDUM)
        if(r1.le.PP)then
                nc=nc+1
                Prob=DCD(i)*DCD(j)      ! Only for CP, contact may be avoided by App user
                r2=ran2(IDUM)
                if(r2.le.Prob)then
                    
                    call CNTG(IDUM,i,j,N0,PRM,ATR,R0,BT,SAI,RPI,RPA,  &
                            CTT,STT,ADIC)
                    CNT(i,1)=CNT(i,1)+1 
                    CNT(j,1)=CNT(j,1)+1
                    
                    CNT2(i,1)=CNT2(i,1)+C1W(j)+FMI(j)-1.    ! soc + fml contacts of j
                    CNT2(j,1)=CNT2(j,1)+C1W(i)+FMI(i)-1.    !              "" "" of i
                    
                    ia=DWL(i)*DWL(j)
                    if(ia.eq.1)then             ! i & j have the App
                        r3=ran2(IDUM)
                        if(r3.le.0.85)then      ! 85% effect. for contact detection
                            CNT(i,2)=CNT(i,2)+1 
                            CNT(j,2)=CNT(j,2)+1
                            
                            CNT2(i,2)=CNT2(i,2)+C1W(j)+FMI(j)-1. ! soc + fml contacts of j
                            CNT2(j,2)=CNT2(j,2)+C1W(i)+FMI(i)-1. !              "" "" of i
                        endif
                    endif
                endif
        endif
    endif
    enddo 
  END  
  
  
!   STT(n)=4   Cured
!   STT(n)=3   Susceptible
!   STT(n)=2   Exposed  :   No contagia | No tiene síntomas
!   STT(n)=1   Asymptom :      Contagia | No tiene síntomas (Portador Asintomático)
!   STT(n)=0   Infected :      Contagia | Tiene síntomas    (Portador Sintomático)
!   STT(n)=-1  Dead

  SUBROUTINE CNTG(IDUM,i,j,N0,PRM,ATR,R0,BT,SAI,RPI,RPA,  &
                  CTT,STT,ADIC)
  implicit real(a-h, o-z)
  external ran2
  real*8 ran2
  integer STT(N0), R0(N0,2), CTT(N0,299), BT(N0), SAI(N0), ADIC(0:105,0:105,4)
  real ATR(N0,9)
  common/VL/Pa1, Pa2, Pa3, xnPD
  
  mi=STT(i); mj=STT(j)
  
                is=0; ih=0
                if(mi.eq.0.or.mi.eq.1)then
                    is=i                        ! is = sick  (I or
                elseif(mi.eq.3)then
                    ih=i                        ! ih = healthy (S)
                endif
                
                if(mj.eq.0.or.mj.eq.1)then
                    is=j
                elseif(mj.eq.3)then
                    ih=j
                endif
                
  ihs=ih*is
  if(ihs.gt.0)then            ! one I or A    +    one S
        
        x=(float(SAI(is))-ATR(is,4))/xnPD +0.1   ! steps after infect. - incub.
!       if(x.le.0.)print*,"ZERO ALARM!!!",x
        Pcntg=ATR(is,3)/(x*Pa1)*exp(-(log(x)-Pa2)**2/Pa3)
        
        v=ATR(i,1)*ATR(j,1)
        if(v.lt.1.E-6)Pcntg=Pcntg*PRM               ! at least one in quarantine
        
!         Pcntg=0.10  !MEGAWARNING erase
        
        r=ran2(IDUM)
        if(r.lt.Pcntg)then
                STT(ih)=2
                if(STT(is).eq.0)then
                    R0(is,1)=R0(is,1)+1     ! I
                else
                    R0(is,2)=R0(is,2)+1     ! A 
                endif
                BT(is)=BT(is)+1
                jj=BT(is)
!                                         if(jj.gt.299)then   !MEGAWARNING
!                                             print*,"No way... Matrix dimension exceeded!"
!                                         endif
                CTT(is,jj)=ih           ! ID of agent infected by "is" 
                    kh=nint(ATR(ih,8)); ks=nint(ATR(is,8))
                    ADIC(kh,ks,2)=ADIC(kh,ks,2)+1           ! ADIC soc-real
                    
!                     if(STT(is).eq.0)then                    !ks = I
!                         P=RPI
!                     else
!                         P=RPA
!                     endif
!                     r2=ran2(IDUM)                    
!                     if(r.lt.P)then
!                         ADIC(kh,ks,1)=ADIC(kh,ks,1)+1       ! ADIC soc-reported
!                     endif
        endif 
        
  endif
  END
  
  
  
  

!   SUBROUTINE SuperSP(IDUM,i,j,N0,PRM,ATR,R0,BT,   CTT,STT)
!   implicit real(a-h, o-z)
!   external ran2
!   real*8 ran2
!   integer STT(N0), R0(N0,2), CTT(N0,299), BT(N0)
!   real ATR(N0,9)
!   
!     do i=1,N0
!     if(STT(i).ge.0)then
!     ii=int(POS(i,1)/dM)+1; jj=int(POS(i,2)/dM)+1
!     if(ii.gt.MU)ii=MU
!     if(jj.gt.MU)jj=MU
!     k=ii+(jj-1)*MU   ! Cell where i lives
! 
!     do l=1,9
!     ic=CL(k,l)      ! All cells surrounding cell k + cell k
!     np=nCL(ic)
!     
!     do ip=1,np
!     j=iCL(ic,ip)    ! How many particles are in k
!     if(j.ne.i.and.Rij(i,j).eq.0)then
!     Rij(i,j)=1; Rij(j,i)=1
! 
!         if(STT(j).ge.0)then
!   
!   END
  
  
!   PRM=1.                ! Prob. of Contagion for outsider-qnt contact (visitas)
!   PCA=0.              ! Prob. of Contagion without symptoms

!   STT(n)=4   Recovered
!   STT(n)=3   Susceptible
!   STT(n)=2   Exposed  
!   STT(n)=1   Asymptomatic
!   STT(n)=0   Symptomatic 
!   STT(n)=-1  Dead
  
  SUBROUTINE Evol(IDUM,ist,N0,NIS,STT,ST0,STD,SPS,RST,MOV,ATR,QNT,  &
                  R0,SAT,SAI,ICU)
  implicit real(a-h, o-z)
  character*6 QNT
  integer STT(N0),SAI(N0),SAT(N0),RST(N0),ICU(N0),MOV(N0),R0(N0,2),ST0(N0)
  integer SPS(N0),STD(N0)
  real ATR(N0,9)
  real*8 ran2
  external ran2
  common/Probs/PSP,PCA,SCF,PRM,PCT,PAE,PTD,MIC,nPD,DLY
  
  do i=1,N0
        
        if(STT(i).ne.3)SAI(i)=SAI(i)+1
        
        if(STT(i).eq.2)then                     ! i = Exposed
            
            m=nint(ATR(i,4))         
            if(SAI(i).eq.m)then                 ! End of Incub. period
                    k=STD(i)    ! I (k=0) or A (k=1)
                    STT(i)=k
                    ST0(i)=k
                    kk=k+1
                    R0(i,kk)=0
!                     r=ran2(IDUM)
!                     if(r.lt.PSN)then            ! I
!                         if(SPS(i).eq.1)then
!                             ATR(i,3)=SCF*PC_ws
!                         else
!                             ATR(i,3)=PC_ws
!                         endif
!                         R0(i,1)=0
!                     else                        ! A
!                         if(SPS(i).eq.1)ATR(i,3)=SCF*PCA
!                         R0(i,2)=0
!                     endif
            endif
         
        elseif(STT(i).eq.0.or.STT(i).eq.1)then  ! i = I or A
            
            ms=nint(ATR(i,4)+PSP*nPD)       ! Incub. period + 2.5 days (Sympt. onset)
            
            n=nint(ATR(i,4)+0.5*ATR(i,5))   ! Incub. + half infect. periods
            m=nint(ATR(i,4)+ATR(i,5))       ! Incub. + Infect. periods
            if(SAI(i).eq.m)then
                    if(STT(i).eq.1)then         ! i = A
                        STT(i)=4                ! i = R
                    else                        ! i = I
                        r=ran2(IDUM)
                        if(r.lt.ATR(i,2))then
                            STT(i)=-1           ! i = M
                            ATR(i,1)=0.
                        else
                            STT(i)=4                ! i = R
                        endif
                        if(ICU(i).eq.1)ICU(i)=0     ! ICU free again
                    endif
            elseif(STT(i).eq.0.and.SAI(i).eq.n)then ! I halfway to healing
                    r=ran2(IDUM)
                    if(r.lt.ATR(i,6))then           ! If i needs ICU
                        NIC=sum(ICU(:))+1
                        
                        
                        if(NIC.gt.MIC)then          ! No ICU available, i dies
                                STT(i)=-1
                                ATR(i,1)=0.
                                NIS=NIS+1           ! extra ICU needed
                        else                        ! if NIC<=MIC
                            ICU(i)=1
                        endif
                        
                        
                        
                        
!                         if(NIC.gt.MIC)then          ! No ICU available, i dies
! !                             r=ran2(IDUM)
! !                             rr=float(N0)/5000.
! !                             if(r.lt.rr)then
!                                 STT(i)=-1
!                                 ATR(i,1)=0.
!                                 NIS=NIS+1           ! extra ICU needed
! !                             endif
!                         elseif(NIC.eq.MIC)then  
!                             r=ran2(IDUM)
!                             rr=float(N0)/5000.
!                             if(r.gt.rr)then
!                                 STT(i)=-1
!                                 ATR(i,1)=0.
!                             else 
!                                 ICU(i)=1
!                             endif
!                         else                        ! if NIC<MIC
!                             ICU(i)=1
!                         endif
                    endif
            endif
            
            if(STT(i).eq.0.and.SAI(i).eq.ms)then ! Symtoms start -> QNT (unless autoexcluded)
                        if(QNT.ne."none")then 
                            r=ran2(IDUM)
                            if(r.ge.PAE)then
                            ATR(i,1)=0.
                            ! ATR(i,3) contagion prob. doesn't change. See SUBR. CNTG: QNT Permeab.
                            ! is triggered if one of the two agents is not moving 
                            endif
                            if(QNT.eq."etar".and.MOV(i).eq.0)MOV(i)=1
                        endif       
            endif  

!             ! R is free if its MOV=1
!             ! Gain it after recovering in scenario: QNT=etar
!             ! It does not if it was Asymptomatic not detected in QNT=etar
!             if(STT(i).eq.4.and.MOV(i).eq.1.and.SAI(i).eq.m)ATR(i,1)=1.
        endif
        
        m=nint(ATR(i,4)+ATR(i,5))
        if(STT(i).eq.4.and.MOV(i).eq.1.and.SAI(i).eq.m)ATR(i,1)=1. !ATR(i,7)
        
        mr=m+nPD*7     ! 7 days after Infec. Period  -> Reset R0
        if(SAI(i).ge.mr)R0(i,:)=-1
  enddo
  END
  
  
  
  SUBROUTINE FMLctg(IDUM,N0,Nfm,NctgF,STT,PRM,FMLm,FMLi,ATR,R0,BT,SAI,CTT,  &
                    RPI,RPA,ADIC)
  implicit real(a-h, o-z)
  integer STT(N0),BT(N0),CTT(N0,299),FMLm(N0),FMLi(N0,9),R0(N0,2)
  integer ADIC(0:105,0:105,4),SAI(N0)
  real ATR(N0,9)
  real*8 ran2
  external ran2 
  common/VL/Pa1, Pa2, Pa3, xnPD
  
  do k=1,Nfm                ! which family?
        Nm=FMLm(k)          ! how many members?
        do l1=1,Nm-1
            i=FMLi(k,l1); mi=STT(i)
            do l2=l1+1,Nm
                j=FMLi(k,l2); mj=STT(j)
                
                
                is=0; ih=0
                if(mi.eq.0.or.mi.eq.1)then
                    is=i
                elseif(mi.eq.3)then
                    ih=i
                endif
                
                if(mj.eq.0.or.mj.eq.1)then
                    is=j
                elseif(mj.eq.3)then
                    ih=j
                endif
                
                ihs=ih*is
                if(ihs.gt.0)then            ! one I or A    +    one S
                        
                    x=(float(SAI(is))-ATR(is,4))/xnPD +0.1   ! steps after infect. - incub.
                    Pcntg=ATR(is,3)/(x*Pa1)*exp(-(log(x)-Pa2)**2/Pa3) !*0.52
                    v=ATR(i,1)*ATR(j,1)
                    if(v.lt.1.E-6)Pcntg=Pcntg*PRM   ! at least one in quarantine
                    
!                     Pcntg=0.8 !MEGAWARNING
                    r=ran2(IDUM)
                    if(r.lt.Pcntg)then
                            STT(ih)=2                   ! E
                            NctgF=NctgF+1
                            if(STT(is).eq.0)then
                                R0(is,1)=R0(is,1)+1     ! I
                            else
                                R0(is,2)=R0(is,2)+1     ! A 
                            endif
                            BT(is)=BT(is)+1
                            jj=BT(is)
                            CTT(is,jj)=ih           ! ID of agent infected by "is" 
                                kh=nint(ATR(ih,8)); ks=nint(ATR(is,8))
                                ADIC(kh,ks,4)=ADIC(kh,ks,4)+1           ! ADIC fml-real
                                
!                                 if(STT(is).eq.0)then                    !ks = I
!                                     P=RPI
!                                 else
!                                     P=RPA
!                                 endif 
!                                 r1=ran2(IDUM)
!                                 if(r1.lt.P)then
!                                     ADIC(kh,ks,3)=ADIC(kh,ks,3)+1       ! ADIC fml-reported
!                                 endif
                    endif 
                            
                endif
            enddo
        enddo
  enddo  
  END
 
 
!   STT(n)=4   Recovered
!   STT(n)=3   Susceptible
!   STT(n)=2   Exposed  
!   STT(n)=1   Asymptomatic
!   STT(n)=0   Symptomatic 
!   STT(n)=-1  Dead
  
  !------------------------ digital CT (85% Effectivity) | PAE=0 for detected infected contacts
  SUBROUTINE mkCT(i,N0,IDUM,rec,CTT,STT,DWL,MOV,BT,Tree,QNT,FMLk,  ADIC,RGS,ATR)
  implicit real(a-h, o-z)
  character*6 QNT
  integer MOV(N0),BT(N0),CTT(N0,299),DWL(N0),Tree(4),RGS(N0),STT(N0),ADIC(0:105,0:105,4)
  integer FMLk(N0),rec
  real ATR(N0,9)
  real*8 ran2
  external ran2
  common/Probs/PSP,PCA,SCF,PRM,PCT,PAE,PTD,MIC,nPD,DLY
  
    !--------------- Isolate i and its contacts (regardless of PRM) 
    !---------------CT1w
    if(STT(i).le.2)ATR(i,1)=0.  ! isolate only if still infectious
!     ATR(i,1)=0.
    
    if(DWL(i).eq.1)then
        N1=BT(i)
        do j1=1,N1
            i1=CTT(i,j1)
            if(DWL(i1).eq.1)then
                r=ran2(IDUM)
                if(r.le.0.85)then       ! 85% of contacts detected
                    
                    kh=nint(ATR(i1,8)); ks=nint(ATR(i,8))
                    k1=FMLk(i1); k2=FMLk(i)
                    if(k1.eq.k2)then                        ! Same family
                        ADIC(kh,ks,3)=ADIC(kh,ks,3)+1       ! ADIC fml-reported VAMA
                    else
                        ADIC(kh,ks,1)=ADIC(kh,ks,1)+1       ! ADIC soc-reported VAMA
                    endif
                    
!                       r=ran2(IDUM)
!                       if(r.ge.PAE)then
                    if(STT(i1).le.2)ATR(i1,1)=0.
                    
!                     ATR(i1,1)=0.
!                         ATR(i1,3)=PRM
!                         if(RGS(i1).eq.0)RGS(i1)=1   ! WARNING: Do we need RGS?
!                       endif
                    if(QNT.eq."etar".and.MOV(i1).eq.0)MOV(i1)=1
                
                    !------------------- Recursive CT   CT2w
                    if(rec.eq.1)then
                        N2=BT(i1)
                        do j2=1,N2
                                i2=CTT(i1,j2)
                                if(DWL(i2).eq.1)then
                                    r=ran2(IDUM)
                                    if(r.le.0.85)then       ! 85% of contacts detected
                                    if(STT(i2).le.2)ATR(i2,1)=0.
!                                     ATR(i2,1)=0.
    !                                 if(RGS(i2).eq.0)RGS(i2)=1
                                    endif
                                endif
                        enddo 
                    endif
                    
                endif
            endif
            
!                 !------------------------------ Tree for all contag.
!                 Tree(1)=Tree(1)+1
! !                     
!                 N2=BT(i1)
!                 do j2=1,N2
!                         Tree(2)=Tree(2)+1
!                         
!                         i2=CTT(i1,j2)
!                         N3=BT(i2)
!                         do j3=1,N3
!                                 Tree(3)=Tree(3)+1
!                                 
!                                 i3=CTT(i2,j3)
!                                 N4=BT(i3)
!                                 do j4=1,N4
!                                         Tree(4)=Tree(4)+1
!                                 enddo
!                         enddo
!                 enddo
!                 !------------------------------
        enddo
    endif
  END

  
  
  !------------------------ manual CT (40% Effectivity) | PAE=0 for detected infected contacts
  SUBROUTINE mkCTm(i,N0,IDUM,rec,CTT,STT,DWL,MOV,BT,Tree,QNT,FMLk,  ADIC,RGS,ATR)
  implicit real(a-h, o-z)
  character*6 QNT
  integer MOV(N0),BT(N0),CTT(N0,299),DWL(N0),Tree(4),RGS(N0),STT(N0),ADIC(0:105,0:105,4)
  integer FMLk(N0),rec
  real ATR(N0,9)
  real*8 ran2
  external ran2
  common/Probs/PSP,PCA,SCF,PRM,PCT,PAE,PTD,MIC,nPD,DLY
  
    !--------------- Isolate i and its contacts (regardless of PRM) 
    !---------------CT1w
    if(STT(i).le.2)ATR(i,1)=0.  ! isolate only if still infectious
!     ATR(i,1)=0.
    
    N1=BT(i)
    do j1=1,N1
        i1=CTT(i,j1)
        r=ran2(IDUM)
        if(r.le.0.4)then        ! 40% of contacts detected
        
                kh=nint(ATR(i1,8)); ks=nint(ATR(i,8))
                k1=FMLk(i1); k2=FMLk(i)
                if(k1.eq.k2)then                        ! Same family
                    ADIC(kh,ks,3)=ADIC(kh,ks,3)+1       ! ADIC fml-reported VAMA
                else
                    ADIC(kh,ks,1)=ADIC(kh,ks,1)+1       ! ADIC soc-reported VAMA
                endif
                    
!                       r=ran2(IDUM)
!                       if(r.ge.PAE)then
                if(STT(i1).le.2)ATR(i1,1)=0.
!                 ATR(i1,1)=0.
!                         ATR(i1,3)=PRM
!                         if(RGS(i1).eq.0)RGS(i1)=1
!                       endif
                if(QNT.eq."etar".and.MOV(i1).eq.0)MOV(i1)=1
            
                !------------------- Recursive CT   CT2w
                if(rec.eq.1)then
                        N2=BT(i1)
                        do j2=1,N2
                                i2=CTT(i1,j2)
                                if(DWL(i2).eq.1)then
                                    r=ran2(IDUM)
                                    if(r.le.0.4)then        ! 40% of contacts detected
                                    if(STT(i2).le.2)ATR(i2,1)=0.
!                                     ATR(i2,1)=0.
    !                                 if(RGS(i2).eq.0)RGS(i2)=1
                                    endif
                                endif
                        enddo
                endif
                
        endif
    enddo
  END
  
  
  
  !------------------------ % Population response to App warnings
  SUBROUTINE SMS(IDUM,N0,nPD,ist,rec,CNT,CNT2,C1W,C2W,FMI,FMLk,FMLi,CMX,DWL,DWLp,DCDp,  DCD)
  implicit real(a-h, o-z)
  external ran2
  real*8 ran2
  integer rec,CNT(N0,2),DWL(N0),FMI(N0),FMLk(N0),FMLi(N0,9)
  real DCD(N0),DCDp(N0),CNT2(N0,2),CMX(2),C1W(N0),C2W(N0)
  
  if(rec.eq.0)then
    C2=1.E6                                         ! CP1w
  else
    x=(CMX(1)-2.65)*DWLp
    y=9./(1.+exp(-2.23685*(x+0.398333)))+4.46229    ! Sigmoidea
    C2=0.98*(y*DWLp-(1.-DWLp)*2.4)                  ! CP2w
  endif
  C1=CMX(1)*DWLp*0.85
!   C2=25.*DWLp*0.85    !MEGAWARNING testing
  
  do i=1,N0
        !--------------------------------------------- Total Contacts
        m=FMI(i)
        r=float(m)-1.
        Cfml=0.03*r*CMX(1)
        Csoc=float(CNT(i,2)*nPD)/float(ist)
        Ctot=Csoc+Cfml
        
        !--------------------------------------------- recursive Total Contacts 
        ! Sum all soc contacts of family members of i
        rCfml=0.
        k=FMLk(i)
        do i2=1,m
        j=FMLi(k,i2)
        rCfml=rCfml+float(CNT(j,2)*nPD)/float(ist)
        enddo
        rCsoc=CNT2(i,2)*float(nPD)/float(ist)
        rCtot=rCfml+rCsoc
        
        !--------------------------------------------- Compare with Contact Threshold
        if(Ctot.ge.C1.or.rCtot.ge.C2)then
            if(DWL(i).eq.1)then
                DCD(i)=DCDp(i)
            else
                DCD(i)=1.
            endif
        else
            DCD(i)=1.
        endif
        
        C1W(i)=Csoc
        C2W(i)=rCsoc
  enddo
  
!             print*,"Family size                 Fac  (C1=Fac*Cmax)"
!             do i=1,8
!             x=float(i)-1.
!             y=0.25*exp(-0.4*x)+0.75
!             print*,x+1, y
!             enddo
!             stop
  END
  
  
  
!   STT(n)=4   Recovered
!   STT(n)=3   Susceptible
!   STT(n)=2   Exposed  
!   STT(n)=1   Asymptomatic
!   STT(n)=0   Symptomatic 
!   STT(n)=-1  Dead

  SUBROUTINE DATthis(ist,mpf,N0,MRT,MIC,NIS,Tree,ICU,nPD,STT,  &
                     SPS,SAI,ATR,RGS,R0,CTA)
  implicit real(a-h, o-z)
  integer STT(N0),kc(0:5),ICU(N0),R0(N0,2),SAI(N0),Tree(4),SPS(N0),RGS(N0)
  real ATR(N0,9), p(0:5), pTree(4)
  
        kc=0; kn=0
        NI=0;  NA=0;  NS=0;   R1t=0.; R2t=0.; Rsp=0.; Rtt=0.; Nt=0
        NIF=0; NAF=0; NCF=0;  RAF=0.; RIF=0.; RCF=0.
        CTA=N0; iar=0
        
        do i=1,N0
        
            j=STT(i)
        
            !---- Accumulated Contagion [1]: R_tot R_A R_I R_sps
            if(j.eq.4.or.j.eq.-1)then
            
!                         if(R0(i,1).ge.0)then 
!                             NI=NI+1
!                             R1t=R1t+R0(i,1)
!             !                 R0(i,1)=-1
!                         endif
!                         if(R0(i,2).ge.0)then
!                             NA=NA+1
!                             R2t=R2t+R0(i,2)
!             !                 R0(i,2)=-1
!                         endif
                rt=max(R0(i,1),R0(i,2))
                if(rt.ge.0)then
                    Nt=Nt+1
                    Rtt=Rtt+rt
                endif
                
                if(SPS(i).eq.1)then
                    NS=NS+1
                    Rsp=Rsp+sum(R0(i,1:2))+1
                endif
                
                !---- Instant Finished Contagion [1]
                nn=ATR(i,4)+ATR(i,5)+mpf
                if(SAI(i).lt.nn)then
                    NCF=NCF+1
                    if(R0(i,1).ge.0)then
                    NIF=NIF+1
                    RIF=RIF+R0(i,1)
                    endif
                    if(R0(i,2).ge.0)then
                    NAF=NAF+1
                    RAF=RAF+R0(i,2)
                    endif
                endif
            endif
            

            
            !---- Contagiados EIA y por separado | Contagios nuevos diarios
            if(j.ge.0.and.j.le.2)then
                kc(3)=kc(3)+1  ! E+A+I actuales
                kc(j)=kc(j)+1  ! E, A, I (separated)
                if(SAI(i).lt.nPD)kn=kn+1 ! E+A+I nuevos diarios
            endif
            
            !---- Contagios totales acumulados: N0-S
            if(j.eq.3)CTA=CTA-1
            
            !---- Moviles      
            if(ATR(i,1).gt.1.E-4)kc(4)=kc(4)+1
           
            !---- Dead
            if(j.eq.-1)kc(5)=kc(5)+1
            
!             !---- Lethality from rgistered
!             if(RGS(i).eq.1)iar=iar+1

        enddo
        
        MRT=kc(5)

        do j=0,5
            p(j)=100.*float(kc(j))/float(N0)
        enddo
    
        !---- ICU
        if(NIS.gt.0)then
            dICU=float(MIC+NIS)
        else
            dICU=float(sum(ICU(:)))
        endif
        picu=100.*dICU/float(N0)
        NIS=0
        
        !---- Contagios totales acumulados
        CTA=100.*CTA/float(N0)
        
!                     !---- Accumulated Contagion [2]: R_tot R_A R_I R_sps
!                     NINA=NI+NA
!                     if(NINA.ne.0)then
!                         wi=float(NI)/float(NINA)
!                         wa=float(NA)/float(NINA)
!                         Rp0=R1t+R2t
!                             if(NI.ne.0)then
!                                 R1t=R1t/float(NI)
!                             else
!                                 R1t=0.
!                             endif
!                             if(NA.ne.0)then
!                                 R2t=R2t/float(NA)
!                             else
!                                 R2t=0.
!                             endif
!                         Rp=wi*R1t+wa*R2t
!                         if(Rp0.gt.0.)then
!                             Rsp=100.*Rsp/Rp0     ! % of infect. by Sps. wrt Total infect.
!                         else
!                             Rsp=0. !; print*,"Ajáa",ist
!                         endif
!                     else
!                         Rp=0.; Rsp=0.
!                     endif

        !---- R accumulated:
        if(Nt.gt.0)then
            Rp=Rtt/float(Nt)
        else
            Rp=0.
        endif
        
        !---- Instant Finished Contagion [2]
        if(NCF.ne.0)then
            wi=float(NIF)/float(NCF)
            wa=float(NAF)/float(NCF)
                if(NIF.ne.0)then
                    RIF=RIF/float(NIF)
                else
                    RIF=0.
                endif
                if(NAF.ne.0)then
                    RAF=RAF/float(NAF)
                else
                    RAF=0.
                endif
            RCF=wi*RIF+wa*RAF
        else
            RCF=0.
        endif
        
        
        
        pn=100.*float(kn)/float(N0)
        
        
        !---- Lethality
        if(iar.eq.0)then
            plt=0.
        else
            plt=100.*float(kc(5))/float(iar)
        endif
        
!         !---- Contagios of nst wave wrt 1st wave
!         if(Tree(1).eq.0)then
!             pTree=0.
!         else
!             pTree(:)=float(Tree(:))/float(Tree(1))
!         endif
        
        
        
        
        
        days=float(ist)/float(nPD)
!         !----------------------------- Contagios nuevos diarios
!         nn=mod(ist,nPD)
!         if(nn.le.10)write(10,*)days, pn
        
        !----------------------------- Checks
!         write(10,*)days, CTA, Rp, Rsp    ! R2t, p(3) !, pTree(:)
        write(10,*)days, CTA, p(5), Rp, picu, p(3), pn
        
!         !----------------------------- Final:
!         write(10,*)days, p(3), CTA, pn, p(5), picu, RCF, Rp !, p(2), p(0), p(1)
!output:  days | E+I+A | Cntg acc| Cntg new | Mort | ICU | R | k | E | I | A 
  END
  
  
  
  
  
    
  SUBROUTINE DATthis2(N0,ist,nPD,ST0,FMI,FMLk,FMLi,CMX,CNT,CNT2,C1W,C2W,ATR)
  implicit real(a-h, o-z)
  integer ST0(N0),CNT(N0,2),FMI(N0),FMLk(N0),FMLi(N0,9)
  real ATR(N0,9),CTG(0:105),C1W(N0),C2W(N0),CMX(2),CNT2(N0,2)
! CTG(i): Incidence by age
  
        !-------- Average Contacts by Age
        do i=1,N0
            !--------------------------------------------- Total Contacts
            m=FMI(i)
            r=float(m)-1.
            Cfml=0.03*r*3.6 !CMX(1)
            Csoc=float(CNT(i,2)*nPD)/float(ist)     ! App
            Cs00=float(CNT(i,1)*nPD)/float(ist)     ! Real
            Ctot=Csoc+Cfml
            Ct00=Cs00+Cfml
            
            !--------------------------------------------- recursive Total Contacts 
            ! Sum all soc contacts of family members of i
            rCfml=0.; rCf00=0.
            k=FMLk(i)
            do i2=1,m
            j=FMLi(k,i2)
            rCfml=rCfml+float(CNT(j,2)*nPD)/float(ist)     ! App
            rCf00=rCf00+float(CNT(j,1)*nPD)/float(ist)     ! Real
            enddo
            rCsoc=CNT2(i,2)*float(nPD)/float(ist)
            rCs00=CNT2(i,1)*float(nPD)/float(ist)
            rCtot=rCfml+rCsoc
            rCt00=rCf00+rCs00
 
            write(11,*)ATR(i,8),   Ct00, rCt00
!             write(11,*)ATR(i,8),   Ctot/0.85, rCtot/0.85
!             !            age      CNTsoc_app  CNTtot_app  rCNTtot_app   CNTsoc_real 
!             write(11,*)ATR(i,8),    CsocApp,    C1W(i),      C2W(i),       Csoc
!             write(11,*)ATR(i,8),    CsocApp,    C1W(i),      C2W(i),       Csoc/5.44 !WARNING
        enddo 
        
        !-------- Average Contagion by Age
        CTG=0.
        do i=1,N0
            if(ST0(i).eq.0.or.ST0(i).eq.1)then
            iAge=nint(ATR(i,8)); CTG(iAge)=CTG(iAge)+1
            endif
        enddo 
        CTGtot=sum(CTG(:))
        ! Print values every 4 years
        do i=0,100,4
!             CTGrel=CTG(i)/CTGtot
!             write(12,*)i, CTGrel
            CTGrel=sum(CTG(i:i+3))/CTGtot
            write(12,*)i, CTGrel
        enddo

                !-------- Average REPORTED Contagion by Age (only symptomatic)
                CTG=0.
                do i=1,N0
                    if(ST0(i).eq.0)then
                    iAge=nint(ATR(i,8)); CTG(iAge)=CTG(iAge)+1
                    endif
                enddo 
                CTGtot=sum(CTG(:))
                do i=0,100,4
                    CTGrel=sum(CTG(i:i+3))/CTGtot
                    write(13,*)i, CTGrel
                enddo
                
  END
            
  

  

  SUBROUTINE Stats(IDUM,N0,nPD,DOI,PCA,  STD,ATR) 
  implicit real(a-h, o-z)
  external ran2
  real*8 ran2
  integer STD(N0)
  real ATR(N0,9), PSN(0:105), leth(0:105), ICUd(0:105), EfcC(0:105),Page(0:105)
  common/CPDp/aa,bb
  
!   open(80,file="Results/Stats/letalidad-fit.dat")
  open(80,file="Stats.dat")
    
!---------------------------- Mortality distribution by age   ATR(i,2)
!---------------------------- ICU need  distribution by age   ATR(i,6)
!   link1: https://ourworldindata.org/uploads/2020/03/COVID-CFR-by-age.png

! -------------------------- Atributes --------------------------
!   Get distribution function of:
!   Sort speed           ATR(:,1) 
!   Death rate           ATR(:,2)
!   Prob. of contagion   ATR(:,3)
!   Incubation period    ATR(:,4)
!   Infection  period    ATR(:,5)
!   Prob of Need ICU     ATR(:,6)
!   Contact probability  ATR(;,7)
!   Age                  ATR(:,8)
!   Test day for Asymp   ATR(:,9) 

  pi=acos(-1.)
  dx=1.
  read(80,*)
  do i=0,105
!   read(80,*)x, Page(i), PSN(i), leth(i), ICUd(i), EfcC(i), yy    ! Schools Opened
    read(80,*)x, Page(i), PSN(i), leth(i), ICUd(i), yy, EfcC(i)    ! Schools Closed
        !-------------------------------------------------- Closed Schools:
        if(i.le.32)EfcC(i)=EfcC(i)+0.26-0.26*x/32.
        if(i.le.8.)EfcC(i)=EfcC(i)+0.18-0.18*x/8.
        if(i.ge.76.)EfcC(i)=EfcC(i)-0.05*(x-76.)/(105.-78.)
        !-------------------------------------------------- Opened Schools: Need Calibration
!         write(86,*)x,EfcC(i)
  enddo

! !   Fitted from Table S5 of:
! !   https://journals.plos.org/plosmedicine/article?id=10.1371/journal.pmed.0050074
!   do i=0,105
!   x=float(i)
!   EfcC(i)= = 0.27851 + 0.16398 * x - 0.014983 * x**2 + 0.00062203 * x**3 - 1.3527e-05 * x**4 + &
!              1.5844e-07 * x**5 - 9.4746e-10 * x**6 + 2.2718e-12 * x**7
!   enddo


! !                   --------- WARNING: Overwriting EfcC(i)
!                     a1=0.04 ; b1=10.
!                     a2=0.029; b2=1.
!                     do i=0,105
!                         x=float(i)
! !                         EfcC(i)=exp(-log(a1*(x-b1))**2)
!                         if(x.le.35.)then
!                             EfcC(i)=exp(-log(a1*(x-b1))**2)
!                         else
!                             EfcC(i)=exp(-log(a2*(x-b2))**2)
!                         endif
!                     enddo

! !                   --------- WARNING: Overwriting EfcC(i)
!                     a1=0.04 ; b1=1.
!                     a2=0.1  ; b2=-2.
!                     do i=0,105
!                         x=float(i)
!                         EfcC(i)=exp(-log(a1*(x-b1))**2)
! !                         if(x.le.8.)then
! !                             EfcC(i)=exp(-log(a1*(x-b1))**2)
! !                         else
! !                             EfcC(i)=exp(-log(a2*(x-b2))**2)
! !                         endif
!                     enddo
!                                                 stop
    
!------------ Assign ages to agents, & age-dependent features
    do i=1,N0
!       x=0.1*float(i)
10      Age=ran2(IDUM)*105.; j=nint(Age)
        P=Page(j)
        r=ran2(IDUM)
        if(r.lt.P)then
            
            ! Agent speed
            ATR(i,1)=1.
            
            ! Age
            ATR(i,8)=j
            
            ! Efectivity of contact
            ATR(i,7)=EfcC(j)
            
            ! Death rate
            ATR(i,2)=leth(j)
            
            ! Prob. of needing ICU
            ATR(i,6)=ICUd(j)
        else
            goto 10
        endif
    enddo

    
! !------------ Plot several IDRs & get avg values
!     ! Below or equal IDR2
!     DR=0.
!     print*; print*,"Average IDR = %Symptomatic"
! !     do k=500,1000,250
!         k=387
!         do i=1,N0
!             j=ATR(i,8)  ! age
!             DR=DR+0.001*float(k)*PSN(j)
!         enddo
!         DR=DR/float(N0)
!         print*,k, "=", DR
! !     enddo
!     
!     ! Above IDR2
!     DR=0.
!     print*; print*,"Average IDR = Symptomatic + %Asymptomatic"
! !     do k=5,25,5
!         k=244
!         do i=1,N0
!             j=ATR(i,8)  ! age
!             DR=DR+PSN(j)+0.001*float(k)*(1.-PSN(j))
!         enddo
!         DR=DR/float(N0)
!         print*,k, "=", DR
! !     enddo
!     stop
    
!                                     ATR(:,2)=0.1   ! WARNING  
!                                     ATR(:,6)=0.25  ! WARNING  
!     write(12,*)
!     stop

! Incubation period
    do i=1,N0
16      d=ran2(IDUM)*DOI
!         P=0.53*d**2*exp(-log(d)**2)/1.4407
        P=5.645252343/(d*0.5*sqrt(2.*pi))*exp(-(log(d)-1.63)**2/(2.*0.5**2))
        r=ran2(IDUM)
        if(r.lt.P)then
            ATR(i,4)=d*float(nPD) ! Steps of incubation for i
!             write(12,*)d
        else
            goto 16
        endif
        
    enddo
    
!             write(30,*)0.,0.
!             do i=1,200
!             d=float(i)*DOI*1.6/200.
!             P=1./(d*0.5*sqrt(2.*pi))*exp(-(log(d)-1.63)**2/(2.*0.5**2))
!             write(30,*)d,P
!             enddo
!             stop
!     write(12,*)
    
    
    
!   Infection period
    ATR(:,5)=12.*float(nPD) ! Same Infectiousness period for all agents
    
    
    
!   Contagion prob.    
    do i=1,N0
77      x=ran2(IDUM)
!         P=exp(-100.*aa*(x-bb)**2)
        if(x.le.bb)then
            P=0.
        else
            P=exp(-log(aa*(x-bb))**2)
        endif
!         P=exp(-aa**2*x)
!         P=(exp(-aa**2*x)+bb*exp(-(aa*bb)**2*x))/(1.+bb)
!         P=exp(-aa*(x-bb)**2)
!         P=(exp(-(aa*(x-bb))**2)+0.1*exp(-(0.1*aa*(x-bb))**2))/1.1
!         P=aa*x*exp(bb*log(cc*x)**2)
!         P=x*exp(-(aa*(x-bb))**2)/Pmax
!         P=exp(-(aa*(x-bb))**0.8)
!         P=0.9*exp(-(aa*(x-bb))**2)+0.1*exp(-0.05*(aa*(x-bb))**2)
        r=ran2(IDUM)
        if(r.gt.P)goto 77
        ATR(i,3)=x
        
        r=ran2(IDUM)
        j=nint(ATR(i,8))
        if(r.le.PSN(j))then
            STD(i)=0            ! "I"
        else
            STD(i)=1            ! "A"
            ATR(i,3)=ATR(i,3)*PCA
            ATR(i,9)=ran2(IDUM)*7.
        endif
    enddo
    
!                 !-------- Sintomaticity by age
!                 do i=0,104
!                     Sage=0; Ssymp=0
!                     do j=1,N0
!                         iA=nint(ATR(j,8))
!                         if(iA.eq.i)then
!                             Sage=Sage+1
!                             if(STD(j).eq.0)Ssymp=Ssymp+1
!                         endif
!                     enddo
!                     
!                     if(Sage.gt.0)then
!                         rel=Ssymp/Sage
!                         write(14,*)i, rel
!                     endif
!                 enddo
!                 stop
    
!     ATR(:,3)=0. !

!                                 do i=1,N0
! !                                         x=float(i)/float(N0)
! !                                         if(x.le.bb)then
! !                                             P=0.
! !                                         else
! !                                             P=exp(-log(aa*(x-bb))**2)
! !                                         endif
!                                 write(160,*)ATR(i,3)
!                                 enddo
!                                 stop    
    END
    
    
    
    
    
! ---------------------------------------------------- All stats
  SUBROUTINE PrintStats(pi,PSP)
  implicit real(a-h, o-z)
  common/CPDp/aa,bb
  common/VL/Pa1, Pa2, Pa3, xnPD
  
!   ------------- Incubation period
    xi=0.
    xf=30.
    N=200
    
    dx=(xf-xi)/float(N)
    write(30,*)0., 0.
    do i=1,N
        d=xi+dx*float(i)
        P=5.645252343/(d*0.5*sqrt(2.*pi))*exp(-(log(d)-1.63)**2/(2.*0.5**2))
        write(30,*)d, P
    enddo
    
!   ------------- Relative viral load from symptoms onset
    xi=0.
    xf=16.
    N=200
    
    dx=(xf-xi)/float(N)
    write(31,*)-PSP, 0.
    do i=1,N
        d=xi+dx*float(i)
        P=exp(-(log(d)-Pa2)**2/Pa3)/(d*Pa1)
        write(31,*)d-PSP, P
    enddo
    
    
!   ------------- f_4: Maximum viral load distribution
    xi=0.
    xf=0.4  ! 1.0
    N=200
    
    dx=(xf-xi)/float(N)
    do i=1,N
        d=xi+dx*float(i)
        if(d-bb.le.0.)then
            P=0.
        else
            P=exp(-log(aa*(d-bb))**2)
        endif
        write(32,*)d, P
    enddo
    
    
    
!   ------------- f_1, f_2:  
    xi=0.
    xf=25.
    N=200
    
    dx=(xf-xi)/float(N)
    do i=0,N
        x=xi+dx*float(i)
!         f1=exp(-log(0.03*x)**2)
!         f2=0.5*exp(-0.3*x)+0.5
        f2=0.75*exp(-0.1*x**2)+0.25    ! Check in - Contact Prob section
        write(40,*)x, f2
    enddo
!     stop
    
!   ------------- Age distribution
    xi=0.
    xf=105.
    N=200
    
    dx=(xf-xi)/float(N)
    do i=0,N
        x=xi+dx*float(i)
        P=1.0044 - 0.0055344 * x + 0.0002754 * x**2 - 1.0004e-05 * x**3 &
                                + 9.4176e-08 * x**4 - 2.6187e-10 * x**5
        write(41,*)x, P
    enddo
    
  END
  
  
  
  
  
  SUBROUTINE InitState(QNT,IDUM,Nq,N0,NE0,NS0,PQN,RMax,b3,  &
                       DWLp,DWL,DCDp,BT,R0,ATR,MOV,STT,ST0,SPS,SAI,CTT,Tree)
  implicit real(a-h, o-z)
  external ran2
  real*8 ran2
  character*6 QNT
  integer STT(N0),ST0(N0),R0(N0,2),BT(N0),CTT(N0,299),SPS(N0),Tree(4),DWL(N0)
  integer SAI(N0),MOV(N0),RND(N0,2)
  real ATR(N0,9), DCDp(N0)
  common/Probs/PSP,PCA,SCF,PRM,PCT,PAE,PTD,MIC,nPD,DLY
  common/Psc/Nfim(9),fP(9,3),PM(9),SP(9)


  
! -------------------------- Atributes --------------------------
!   Get distribution function of:
!   Sort speed           ATR(:,1) 
!   Death rate           ATR(:,2)
!   Prob. of contagion   ATR(:,3)
!   Incubation period    ATR(:,4)
!   Infection  period    ATR(:,5)
!   Prob of Need ICU     ATR(:,6)
!   Contact probability  ATR(;,7)
!   Age                  ATR(:,8)
!   Test day for Asymp   ATR(:,9) 


!   Movility subjected to speed    MOV(:)
    MOV=1
    

!     ATR(:,3)=PCA   ! if E->I ATR(i,3) changes to PC_ws 
! !------------------------------------------------------------    
! ! Graph Contagion Distribution Functions 
! ! WARNING!!! Turn off for running
!     do k=0,4
!     az=4.+float(2*k)
! !     az=4.
!         do j=0,4
!         bz=0.03+float(j)*0.03
!         Pmax=0.; dP=1.d-2
!             do i=0,100
!             x=float(i)*dP
!             P=x*exp(-(az*(x-bz))**2)
!             if(P.gt.Pmax)Pmax=P
!             enddo
!             
!             do i=0,100
!             x=float(i)*dP
!             P=x*exp(-(az*(x-bz))**2)/Pmax
!             write(160,*)x, P
!             enddo
!             write(160,*)
!         enddo
!     enddo
!     stop
!     
! !------------------------------------------------------------    

!     Pmax=0.; dP=1.d-3
!     do i=0,1000
!     x=float(i)*dP
!     P=x*exp(-(az*(x-bz))**2)
!     if(P.gt.Pmax)Pmax=P
!     enddo  


                ! Make random chain of numbers from 1 to N0 [given by RND(:,1)]
                print*,"  Making a random chain"
                RND=0; j=0; ic=0
                300 i=int(ran2(IDUM)*float(N0))+1
                ic=ic+1
                    
                if(RND(i,2).eq.0)then
                    j=j+1; RND(i,2)=1; RND(j,1)=i
                endif
                    
                if(j.lt.N0)goto 300
                
!                 do i=1,N0
!                 write(20,*)i,RND(i,1)
!                 enddo
                print*,"  Random chain cycles:", ic,real(float(ic)/float(N0))
!                 stop


    if(QNT.eq."none".or.QNT.eq."sint")then
            Nq=0; PQN=0.
    elseif(QNT.eq."obli")then
            Nq=nint(PQN*float(N0))
            do i=1,Nq
            j=RND(i,1)
            ATR(j,1)=0.
!             ATR(1:Nq,3)=ATR(1:Nq,3)*PRM       ! Not PRM*PC_ws
            MOV(j)=0
            enddo
    elseif(QNT.eq."etar")then
            Nq=0
            do i=1,N0
                r=ran2(IDUM)
                if(ATR(i,2).gt.RMax)then
                    Nq=Nq+1
                    ATR(i,1)=0.
!                     ATR(i,3)=ATR(i,3)*PRM  ! Not PRM*PCA
                    MOV(i)=0
                endif
            enddo
            PQN=float(Nq)/float(N0)
    endif
!                                     nin=0; nout=0
!                                     do i=1,N0
!                                         if(ATR(i,2).gt.RMax)then
!                                             if(MOV(i).eq.0)then
!                                             nin=nin+1
!                                             else
!                                             nout=nout+1
!                                             endif
!                                         endif
!                                     enddo
!                                     print*,"nin :", nin
!                                     print*,"nout:", nout
!                                     stop
  
    BT=0
    R0=-1               ! -1 if S, 0 if E
    STT=3               ! all S
    ST0=-10
    SPS=0
    SAI=-1              ! Steps after infection  (I & A start with ATR(i,4) days behind) 
    CTT=0
    Tree=0              ! Waves of contagium
    
! -------------------------- States ------------------------
!   ST0(n)=0   R or M that was I
!   ST0(n)=1   R or M that was A

!   STT(n)=4   R
!   STT(n)=3   S
!   STT(n)=2   E   No contagia | No tiene síntomas
!   STT(n)=1   A      Contagia | No tiene síntomas
!   STT(n)=0   I      Contagia | Tiene síntomas
!   STT(n)=-1  M
    
    if(QNT.ne."etar")then
!             do j=1,NI0
!                 jj=Nq+j
!                 STT(jj)=0        ! I
!                 ST0(jj)=0
! !                 ATR(jj,3)=ATR(jj,3)*PC_ws
!                 R0(jj,1)=0
!                 SAI(jj)=nint(ATR(jj,4))-1   ! I starts with those days behind 
!             enddo
!             do j=1,NA0
!                 jj=Nq+NI0+j
!                 STT(jj)=1    ! A
!                 ST0(jj)=1
!                 R0(jj,2)=0
!                 SAI(jj)=nint(ATR(jj,4))-1
!             enddo
            do j=1,NE0
                jj=RND(Nq+j,1) !+NI0+NA0
                STT(jj)=2    ! E
                r=ran2(IDUM)
                SAI(jj)=nint(r*ATR(jj,4))   ! random day of Incub. Period
            enddo
    else
!             n=0
!             do i=1,N0
!                 if(ATR(i,2).lt.RMax)then
!                     n=n+1
!                     STT(i)=0        ! I
!                     ST0(i)=0
! !                     ATR(i,3)=ATR(i,3)*PC_ws
!                     R0(i,1)=0
!                     SAI(i)=nint(ATR(i,4))-1
!                 endif
!                 if(n.eq.NI0)goto 40
!             enddo
!         40  n=0
!             do i=1,N0
!                 if(ATR(i,2).lt.RMax.and.STT(i).eq.3)then
!                     n=n+1
!                     STT(i)=1        ! A
!                     ST0(i)=1
!                     R0(i,2)=0
!                     SAI(i)=nint(ATR(i,4))-1
!                 endif
!                 if(n.eq.NA0)goto 48
!             enddo 
!         48  n=0
            n=0
            do j=1,N0
                i=RND(j,1)
                if(ATR(i,2).lt.RMax.and.STT(i).eq.3)then
                    n=n+1
                    STT(i)=2        ! E
                    r=ran2(IDUM)
                    SAI(i)=nint(r*ATR(i,4))
                endif
                if(n.eq.NE0)goto 50
            enddo 
    endif
    50 continue
    
                    Nfree=N0-Nq
                    if(NS0.gt.Nfree)then
                    print*,"STOP: NS0 > Nfree"
                    stop
                    endif
    do j=1,NS0
101     i=int(ran2(IDUM)*float(N0))+1
!         if(SPS(i).eq.0.and.STT(i).eq.3)then
        if(SPS(i).eq.0.and.MOV(i).eq.1)then ! Sps are free
            SPS(i)=1
!             ATR(i,3)=1. ! WARNING!!!
!             ATR(i,3)=ATR(i,3)*SCF ! May be we don't need this
!             if(STT(i).eq.0)then
!                 ATR(i,3)=ATR(i,3)*SCF
!             elseif(STT(i).eq.1)then
!                 ATR(i,3)=ATR(i,3)*SCF
!             endif
        else
            goto 101
        endif
    enddo
    
!     !---------------- All Superspr. are superinfected:
!     jsp=0
!     do i=1,N0
!         if(STT(i).eq.0.and.jsp.lt.NS0)then
!             SPS(i)=1
!             jsp=jsp+1
!             ATR(i,3)=0.6 ! Superinfectious
!         endif
!     enddo    
    
!         do i=1,N0
!             if(STT(i).eq.0.or.STT(i).eq.1)print*,"R0",R0(i,:) !"SAI", i, SAI(i)
!         enddo
!         stop

    
! ----------------- Download pCT app
  DWL=0                             ! Didn't download pCT app
  do i=1,N0
        r=ran2(IDUM)
        if(r.le.DWLp)DWL(i)=1       ! Did download it
  enddo
  
! ----------------- SMS DISOBEDIENCE
    do i=1,N0

601     x=ran2(IDUM)
!         P=4.25*x*exp(-6.4*(x-0.1)**2)   ! Bolttzmian
        P=exp(-12.*(x-b3)**2)        ! Gaussian
        r=ran2(IDUM)
        if(r.le.P)then
            DCDp(i)=x
!             write(20,*)x
        else
            goto 601
        endif
    enddo
!                 dx=0.005
!                 do j=0,5
!                     d0=0.2*float(j)
!                     do i=0,200
!                         x=float(i)*dx
!                         P=exp(-12.*(x-d0)**2)
!                         write(20,*)x,P
!                     enddo
!                     write(20,*)
!                 enddo
!                 stop
    
  END
  
  
  
  
  
  SUBROUTINE mkFML(N0,IDUM,Nfm,FMLm,FMLi,FMLk,FMI,ATR)
  implicit real(a-h, o-z)
  integer FMLi(N0,9),FMLk(N0),nb(0:9),Hag(9,3),isat(3),FMLm(N0),ag(N0),FMI(N0)
  real ATR(N0,9),H0(0:105),Had(0:105),Ham(0:105,9)
  real*8 ran2
  external ran2 
  common/Psc/Nfim(9),fP(9,3),PM(9),SP(9)
  
!---------------------- Number of Families (Nfm)
!     PM=0.
!     PM(1)=0.36
!     PM(2)=0.27
!     PM(3)=0.16
!     PM(4)=0.13
!     PM(5)=0.08
    
!   Link: https://www.google.com/url?q=http://demografiasocial.sociales.uba.ar/wp-content/uploads/sites/181/2014/09/Documento-n-5-Arino-Hogares-y-Familias.pdf&sa=U&ved=2ahUKEwjCtMKFit3sAhWyEbkGHRMJArEQFjAGegQIARAB&usg=AOvVaw249ex13BDZZ5RT0ldDrC8h
    PM=0.
    PM(1)=0.18
    PM(2)=0.23
    PM(3)=0.20
    PM(4)=0.18
    PM(5)=0.10
    PM(6)=0.06
    PM(7)=0.02
    PM(8)=0.03
    
! !     f1=PM(1); f2=f1+PM(1); f3=f2+PM(2); f4=f3+PM(4); f5=1.-f4 
!     print*,"F5",f5, sum(PM(:))

    st=0.
    do i=1,9
        st=st+float(i)*PM(i)
    enddo
    Nfm=nint(float(N0)/st)
    
    nn=0
    do i=1,9
        Nfim(i)=nint(float(Nfm)*PM(i))  ! Nfim(m)= How many families with m members
        nn=nn+Nfim(i)*i
    enddo
    hn=nn-N0
    Nfim(1)=Nfim(1)-hn
    Nfm=sum(Nfim(:))    
    
    nn=0; nb(0)=0
    do m=1,9
        nn=nn+Nfim(m)
        nb(m)=nn
!         print*,"Block",m,Nfim(m),nb(m), Nfm
    enddo
    
    do m=1,9
        j1=nb(m-1)+1
        j2=nb(m)
        FMLm(j1:j2)=m
!         print*,"ACAche", j1,j2,Nfm
    enddo
!     stop
!                                 do i=1,9
!                                 print*,real(100.*float(Nfim(i))/float(Nfm))
!                                 enddo    
    
!---------------------- AGE GROUP distribution
!     ! fP = Percentage of c.a.e in each family size
!     fP=0.
!     fP(1,1)=0.05 ;   fP(1,2)=0.55   ;   fP(1,3)=0.40
!     fP(2,1)=0.10 ;   fP(2,2)=0.45   ;   fP(2,3)=0.45
!     fP(3,1)=0.35 ;   fP(3,2)=0.60   ;   fP(3,3)=0.05
!     fP(4,1)=0.50 ;   fP(4,2)=0.40   ;   fP(4,3)=0.10
!     fP(5,1)=0.60 ;   fP(5,2)=0.30   ;   fP(5,3)=0.10
    
    fP=0.
    fP(1,1)=0.05 ;   fP(1,2)=0.55   ;   fP(1,3)=0.40
    fP(2,1)=0.10 ;   fP(2,2)=0.45   ;   fP(2,3)=0.45
    fP(3,1)=0.35 ;   fP(3,2)=0.55   ;   fP(3,3)=0.10
    fP(4,1)=0.50 ;   fP(4,2)=0.40   ;   fP(4,3)=0.10
    fP(5,1)=0.55 ;   fP(5,2)=0.30   ;   fP(5,3)=0.15
    fP(6,1)=0.50 ;   fP(6,2)=0.30   ;   fP(6,3)=0.20
    fP(7,1)=0.45 ;   fP(7,2)=0.35   ;   fP(7,3)=0.20
    fP(8,1)=0.40 ;   fP(8,2)=0.35   ;   fP(8,3)=0.25
    
    ! correct fP according to the percentage of each family size
    SP=0.
    do m=1,8 !5
        SP(1)=SP(1)+fP(m,1)*PM(m)
        SP(2)=SP(2)+fP(m,2)*PM(m)
        SP(3)=SP(3)+fP(m,3)*PM(m)
    enddo
    print*
    do m=1,8 !5
        fP(m,1)=fP(m,1)*PM(m)/SP(1)
        fP(m,2)=fP(m,2)*PM(m)/SP(2)
        fP(m,3)=fP(m,3)*PM(m)/SP(3)
        print*,m, 100.*fP(m,:)
    enddo
    print*

    ! Assign family to agent
    FMLi=0; Hag=0; isat=0
    do i=1,N0
        iag=nint(ATR(i,8))
        if(iag.le.19)then
            ja=1
        elseif(iag.le.65)then
            ja=2
        else
            ja=3
        endif
        ag(i)=ja
        
        
! 100     m=int(ran2(IDUM)*5.)+1
100     m=int(ran2(IDUM)*8.)+1      ! sort family size
!                                     if(m.gt.5)print*,"ACAAanr"
        P=fP(m,ja)
        r=ran2(IDUM)
        if(r.le.P)then
            k1=nb(m-1)+1; k2=nb(m)
            do k=k1,k2
                do j=1,m
                    if(FMLi(k,j).eq.0)then
                        FMLi(k,j)=i             ! i's of each family
                        Hag(m,ja)=Hag(m,ja)+1
                        goto 110
                    endif
                enddo
            enddo
            isat(ja)=isat(ja)+1
!             print*,i,"No space in this Family",ja,m
            goto 100
        else
            goto 100
        endif
110     continue        
    enddo
    print*,"AGE GROUP distribution done..."
    print*,"Saturations of Child Adult Elderly:", isat(:); print*
                
                do m=1,8 !5
                    rh=100.*float(Hag(m,1))/sum(Hag(:,1)) 
                                          x=float(m)-0.5
                    write(21,*)x ,"0"   ; x=float(m)+0.5
                    write(21,*)x ,rh    ; x=float(m)+1.5
                    write(21,*)x ,"0"
                    
                    rh=100.*float(Hag(m,2))/sum(Hag(:,2)) 
                                          x=float(m)+9.5
                    write(21,*)x ,"0"   ; x=float(m)+10.5
                    write(21,*)x ,rh    ; x=float(m)+11.5
                    write(21,*)x ,"0"   
                    
                    rh=100.*float(Hag(m,3))/sum(Hag(:,3)) 
                                          x=float(m)+19.5
                    write(21,*)x ,"0"   ; x=float(m)+20.5
                    write(21,*)x ,rh    ; x=float(m)+21.5
                    write(21,*)x ,"0"   
                enddo
!                 stop
    
    
!---------------------- AGE DIFFERENCE distribution
  
!------------------ Theoretical AD Histogram
!                     P=1.   ; a=0.01               ! 04
!                     Q=0.95 ; b1=0.008 ; b2=32.
!                     R=0.2  ; c1=0.002 ; c2=60.
! 
!                     P=0.8  ; a=0.004              ! 05
!                     Q=0.95 ; b1=0.004 ; b2=32.
!                     R=0.8  ; c1=0.003 ; c2=66.

!                     P=0.8  ; a=0.006              ! 06
!                     Q=0.95 ; b1=0.006 ; b2=32.
!                     R=0.6  ; c1=0.003 ; c2=66.   
                    
!                     P=0.7  ; a=0.006              ! 07
!                     Q=0.98 ; b1=0.006 ; b2=32.
!                     R=0.2  ; c1=0.008 ; c2=64.
                    
!                     P=0.8  ; a=0.01              ! 08
!                     Q=1.0  ; b1=0.016 ; b2=29.
!                     R=0.1  ; c1=0.008 ; c2=64.
                    
                    P=1.00 ; a=0.01              ! 09 Open Schools
                    Q=0.90 ; b1=0.014 ; b2=30.
                    R=0.16 ; c1=0.007 ; c2=60.                    
                    
                    do i=0,105
                        d=float(i)
                        H0(i)=P*exp(-a*d**2)+  Q*exp(-b1*(d-b2)**2)+  R*exp(-c1*(d-c2)**2)
                    enddo
                    area=sum(H0(:))
                    H0=H0/area
!                                         do i=0,105
!                                             write(22,*)i,H0(i)
!                                         enddo

!------------------ Real AD Histogram

        call GetDF(N0,Nfm,FMLm,FMLi,ATR,Had,H0,   DF0)
        print*,"Initial Histogram difference",DF
        
!                                 do i=0,105
!                                     dag=100.*Had(i)/float(N0)
!                                     write(60,*)i, dag
! !                                     write(60,*)i, Had(i),H0(i)
!                                 enddo
        
    DF=1.E6; DF0=DF
!     do while (DF.gt.5.E6) ! WARNING, only for comparison purposes
    do while (DF.gt.1.E-2)
        it=it+1
200     k1=int(ran2(IDUM)*float(Nfm))+1
        m1=FMLm(k1)
        j1=int(ran2(IDUM)*float(m1))+1
        i1=FMLi(k1,j1)
        
        k2=int(ran2(IDUM)*Nfm)+1
        m2=FMLm(k2)
        j2=int(ran2(IDUM)*float(m2))+1
        i2=FMLi(k2,j2)
        
        if(ag(i1).eq.ag(i2))then    ! Only if i1 & i2 are from the same AGE GROUP
            ! switch
            FMLi(k1,j1)=i2
            FMLi(k2,j2)=i1
            
            call GetDF(N0,Nfm,FMLm,FMLi,ATR,Had,H0,   DF)
            
            if(DF.gt.DF0)then
                ! unswitch
                FMLi(k1,j1)=i1
                FMLi(k2,j2)=i2
            else
                DF0=DF
            endif
            
!             write(60,*)it, DF
        else
            goto 200
        endif
    enddo
    print*,"Final Histogram difference:",DF, "steps:",it
                                do i=0,105
!                                     dag=100.*Had(i)/float(N0)
!                                     write(60,*)i, dag
                                    write(60,*)i, Had(i),H0(i)
                                enddo
                                
!                               FMI & FMLk & Age Histogram for each FAMILY SIZE
                                Ham=0. 
                                do k=1,Nfm
                                    m=FMLm(k)
                                    do j=1,m
                                        i=FMLi(k,j)
                                        FMLk(i)=k               ! Which family does i belong to
                                        ia=nint(ATR(i,8))
                                        Ham(ia,m)=Ham(ia,m)+1
                                        FMI(i)=m                ! DO NOT COMMENT THIS
                                    enddo
                                enddo
                                Ham(:,:)=Ham(:,:)/sum(Ham(:,:))
                                
                                do j=1,8 !5
                                    do i=0,105
                                        write(60+j,*)i, Ham(i,j)
                                    enddo
                                enddo
                                
! !                               Print FAMILY 
!                                 do k=1,Nfm
!                                     m=FMLm(k)
!                                     write(90,*)k,m
!                                     do j=1,m
!                                         ia=nint(ATR(i,8))
!                                         write(90,*)FMLi(k,j), ia
!                                     enddo
!                                 enddo
!         stop
  END
  
  
  
  SUBROUTINE GetDF(N0,Nfm,FMLm,FMLi,ATR,Had,H0,   DF)
  implicit real(a-h, o-z)
  integer FMLi(N0,9),FMLm(N0)
  real ATR(N0,9),Had(0:105),H0(0:105)
  
        Had=0.
        do k=1,Nfm
            m=FMLm(k)
            do i1=1,m-1
                i=FMLi(k,i1)
                do i2=i1+1,m
                    j=FMLi(k,i2)
                    dag=abs(ATR(j,8)-ATR(i,8))
                    ii=nint(dag)
                    Had(ii)=Had(ii)+1
                enddo
            enddo
        enddo
        
        area=sum(Had(:))
        Had=Had/area
        
        DF=0.
        do i=0,105
            DF=DF+abs(Had(i)-H0(i))
        enddo
  
  END
  
  
  
  
  
  SUBROUTINE WriteAll(N0,Nfm,FMLm,FMLi,FMLk,FMI,STT,ST0,STD,SAI,SPS,MOV,ATR)
  implicit real(a-h, o-z)
  character*6 QNT
  integer FMLi(N0,9),FMLk(N0),FMLm(N0),FMI(N0)
  integer STT(N0),ST0(N0),STD(N0),R0(N0,2),BT(N0),CTT(N0,299),SPS(N0),DWL(N0)
  integer SAI(N0),MOV(N0)
  real ATR(N0,9), DCDp(N0) !, Had(0:105),H0(0:105)
  common/Probs/PSP,PCA,SCF,PRM,PCT,PAE,PTD,MIC,nPD,DLY
  common/Psc/Nfim(9),fP(9,3),PM(9),SP(9)

  write(94,*)"# STT(i) ST0(i) STD(i) SAI(i) SPS(i) MOV(i) ATR(i,1:9)"
  do i=1,N0
        write(94,*)STT(i),ST0(i),STD(i),SAI(i),SPS(i),MOV(i),ATR(i,1:9),FMLk(i)
  enddo
  
  write(95,*)Nfm
  do k=1,Nfm
        m=FMLm(k)               ! members of family k
        write(95,*)m
        write(95,*)FMLi(k,1:m)  ! id of each members
  enddo
  END
  
  
  
  
  SUBROUTINE ReadAll(IDUM,N0,Nfm,FMLm,FMLi,FMLk,FMI,b3,STT,ST0,STD,SAI,SPS,MOV,   &
                     DWLp,DWL,DCDp,BT,R0,CTT,ATR)
  implicit real(a-h, o-z)
  character*6 QNT
  integer FMLi(N0,9),FMLk(N0),FMLm(N0),FMI(N0)
  integer STT(N0),ST0(N0),STD(N0),R0(N0,2),BT(N0),CTT(N0,299),SPS(N0),DWL(N0)
  integer SAI(N0),MOV(N0)
  real ATR(N0,9), DCDp(N0) !, Had(0:105),H0(0:105)
  real*8 ran2
  external ran2 
  common/Probs/PSP,PCA,SCF,PRM,PCT,PAE,PTD,MIC,nPD,DLY
  common/Psc/Nfim(9),fP(9,3),PM(9),SP(9)
  
  BT=0
  R0=-1               ! -1 if S, 0 if E
!   STT=3               ! all S
!   ST0=-10
!   SPS=0
!   SAI=-1              ! Steps after infection  (I & A start with ATR(i,4) days behind) 
  CTT=0
  
  ! ----------------- Download pCT app
  DWL=0                             ! Didn't download the App
  do i=1,N0
        r=ran2(IDUM)
        if(r.le.DWLp)DWL(i)=1       ! Did download the App
  enddo
  
! ----------------- SMS DISOBEDIENCE
    do i=1,N0
601     x=ran2(IDUM)
!         P=4.25*x*exp(-6.4*(x-0.1)**2)   ! Bolttzmian
        P=exp(-12.*(x-b3)**2)        ! Gaussian
        r=ran2(IDUM)
        if(r.le.P)then
            DCDp(i)=x
!             write(20,*)x
        else
            goto 601
        endif
    enddo
  
  read(94,*)
  do i=1,N0
        read(94,*)STT(i),ST0(i),STD(i),SAI(i),SPS(i),MOV(i),ATR(i,1:9),FMLk(i) 
  enddo
  
  read(95,*)Nfm
  do k=1,Nfm
        read(95,*)m
        FMLm(k)=m
        read(95,*)FMLi(k,1:m)  ! id of each members
        do j=1,m
            i=FMLi(k,j)
            FMI(i)=m            ! Family members of agent i
        enddo
  enddo
  END
  
  
  
  
  
  
  
  FUNCTION ran2(idum)
  INTEGER idum,IM1,IM2,IMM1,IA1,IA2,IQ1,IQ2,IR1,IR2,NTAB,NDIV
  DOUBLE PRECISION ran2,AM,EPS,RNMX
  PARAMETER (IM1=2147483563,IM2=2147483399,AM=1.d0/IM1,IMM1=IM1-1)
  PARAMETER (IA1=40014,IA2=40692,IQ1=53668,IQ2=52774,IR1=12211,IR2=3791)
  PARAMETER (NTAB=32,NDIV=1+IMM1/NTAB,EPS=1.2d-16,RNMX=1.d0-EPS)
  INTEGER idum2,j,k,iv(NTAB),iy
  SAVE iv,iy,idum2
  DATA idum2/123456789/, iv/NTAB*0/, iy/0/
  if (idum.le.0) then
    idum=max(-idum,1)
    idum2=idum
    do 11 j=NTAB+8,1,-1
      k=idum/IQ1
      idum=IA1*(idum-k*IQ1)-k*IR1
      if (idum.lt.0) idum=idum+IM1
      if (j.le.NTAB) iv(j)=idum
11      continue
    iy=iv(1)
  endif
  k=idum/IQ1
  idum=IA1*(idum-k*IQ1)-k*IR1
  if (idum.lt.0) idum=idum+IM1
  k=idum2/IQ2
  idum2=IA2*(idum2-k*IQ2)-k*IR2
  if (idum2.lt.0) idum2=idum2+IM2
  j=1+iy/NDIV
  iy=iv(j)-idum2
  iv(j)=idum
  if(iy.lt.1)iy=iy+IMM1
  ran2=min(AM*iy,RNMX)
  return
  END
  