# SERIA #

SERIA is a Monte Carlo agent-based model that reproduces the essential aspects of social and household contacts in the context of COVID-19. The model includes all agents of the well-known SEIR models, and adds an asymptomatic agent, a fundamental component of the COVID-19 pandemic. SERIA is able to evaluate the performance of CT and CP in terms of FES and mortality rate. 

### Contact ###

* Germán Soldano (<german.soldano@unc.edu.ar>)
* Juan A. Fraire (<juanfraire@unc.edu.ar>)
* Rodrigo Quiroga (<rquiroga@unc.edu.ar>)

### Acknowledgments ###

This research was funded by the "Contact Traceability through the Digital Context of Mobile Devices" (ContactAR) project, awarded by the National Agency for the Promotion of Research, Technological Development and Innovation (I + D + i Agency), under the Ministry of Science, Technology and Productive Innovation of Argentina.

